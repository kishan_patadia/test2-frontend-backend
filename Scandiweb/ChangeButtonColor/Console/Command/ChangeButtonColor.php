<?php
/**
 * Copyright © Scandiweb All rights reserved.
 * See COPYING.txt for license details.
 */
declare (strict_types = 1);

namespace Scandiweb\ChangeButtonColor\Console\Command;

use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ChangeButtonColor extends Command
{

    const HEX_COLOR_ARGUMENT    = "hexcolor";
    const STOREVIEW_ID_ARGUMENT = "storeviewid";

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $_configWriter;

    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    private $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    private $_cacheFrontendPool;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * ChangeButtonColor constructor
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_configWriter      = $configWriter;
        $this->_cacheTypeList     = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->_storeManager      = $storeManager;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $hexcolor    = $input->getArgument(self::HEX_COLOR_ARGUMENT);
        $storeviewid = $input->getArgument(self::STOREVIEW_ID_ARGUMENT);

        $isValidHexColor = $isValidStoreViewID = true;

        if (!$this->isValidHexColor($hexcolor)) {
            $isValidHexColor = false;
            $output->writeln("The HEX Color is invalid. Please try again with valid HEX Color.");
        }
        if (!$this->isValidStoreViewID($storeviewid)) {
            $isValidStoreViewID = false;
            $output->writeln("The Store View ID is invalid. Please try again with valid Store View ID.");
        }

        if ($isValidHexColor && $isValidStoreViewID) {
            $output->writeln("The following configuration has been set successfully.");
            $output->writeln("HEX Color: " . $hexcolor);
            $output->writeln("Store View ID: " . $storeviewid);

            $this->_configWriter->save('changecolor/general/hex_color', $hexcolor, \Magento\Store\Model\ScopeInterface::SCOPE_STORES, $storeviewid);

            $types = ['config', 'block_html', 'full_page'];
            foreach ($types as $type) {
                $this->_cacheTypeList->cleanType($type);
            }
            foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                $cacheFrontend->getBackend()->clean();
            }
        }
    }

    /**
     * @param string $hexcolor
     * @return bool
     */
    public function isValidHexColor($hexcolor)
    {
        if (preg_match('/^[a-fA-F0-9]{6}$/i', $hexcolor)) {
            return true;
        }
        return false;
    }

    /**
     * @param string $storeviewid
     * @return bool
     */
    public function isValidStoreViewID($storeviewid)
    {
        try {
            $store = $this->_storeManager->getStore($storeviewid);
            if ($store->getId()) {
                return true;
            }
            return false;
        } catch (LocalizedException $e) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("scandiweb:color-change")
            ->setDescription("Change button color")
            ->addArgument(self::HEX_COLOR_ARGUMENT, InputArgument::REQUIRED, 'HEX Color')
            ->addArgument(self::STOREVIEW_ID_ARGUMENT, InputArgument::REQUIRED, 'Store View ID');
        parent::configure();
    }
}
